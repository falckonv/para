/* IGC url browser MONGODB STORAGE
   Building on marni/goigc/igc IGC parser
   Author Vanja Falck

Sources used to develop the code:

Testing for API in go-router:
https://github.com/appleboy/gofight

REST-API testing:
https://github.com/gavv/httpexpect

Security for memory ect:
https://github.com/awnumar/memguard

About GET, POST etc with gin router in golang:
https://github.com/gin-gonic/gin#using-get-post-put-patch-delete-and-options

IGC format and files: https://aerofiles.readthedocs.io/en/latest/guide/igc-writing.html
http://skypolaris.org/wp-content/uploads/IGS%20Files/Madrid%20to%20Jerez.igc

Formatting duration in golang
https://stackoverflow.com/questions/47341278/how-to-format-a-duration-in-golang

mgo db
http://labix.org/mgo

routing - did a refactoring of web-handlers by inspiration of this code
https://blog.merovius.de/2017/06/18/how-not-to-use-an-http-router.html

*/

package main

import (
  "os"
  "log"
  "time"
  "net/http"
  "parag/parag"
)

// Latest is the global id counter
// TODO: fetch Latest from DB (current solution is not persistent)
var Latest int

// This is not working:
func handlerRedirect(w http.ResponseWriter, r *http.Request) {
  http.Redirect(w, r, "/paraglider/api", 302)
}
func handlerNotFound(w http.ResponseWriter, r *http.Request) {
  http.NotFound(w, r)
}

func main() {
  // Restarts ID (should be stored in DB for persistence)
  Latest = 1
  parag.LastID = &Latest

  // Start uptime counting when web-server is launched
  parag.UpTimeStart = time.Now()
  parag.GlobalDB    = &parag.MongoTrackDB{"", "trackdb", "trackcollection", "hookcollection"}

  port := os.Getenv("PORT")

  // Dont use this on heroku:
  if port == "" {
  port = "8500"
  }

  // Initiates DB
  parag.GlobalDB.Init()

  /* Initiate with dummies to test:
  s1 := RegTrack{TrID: 1, TrURL: "http://skypolaris.org/wp-content/uploads/IGS%20Files/Madrid%20to%20Jerez.igc", Track: Track{HDate: "2016-02-19 00:00:00 +0000 UTC", Pilot: "Miguel Angel Gordillo", Glider: "RV8", GliderID: "EC-XLL", TrackLength: 0}}
  s2 := RegTrack{TrID: 2, TrURL: "http://skypolaris.org/wp-content/uploads/IGS%20Files/Jarez%20to%20Senegal.igc", Track: Track{HDate: "2016-02-20 00:00:00 +0000 UTC", Pilot: "Miguel Angel Gordillo", Glider: "RV8", GliderID: "EC-XLL", TrackLength: 0}}
     GlobalDB.Add(s1)
     GlobalDB.Add(s2)
  */
    http.HandleFunc("/admin/api/tracks_count/",       parag.HandlerAdminTrackC)
    http.HandleFunc("/admin/api/tracks/",             parag.HandlerAdmin)
    http.HandleFunc("/paraglider/api/igc/",           parag.HandlerRegTrack)
    http.HandleFunc("/paraglider/",                   handlerRedirect)
    http.HandleFunc("/paraglider/api/",               parag.HandlerAPIInfo)
    http.HandleFunc("/paraglider/webhook/new_track/", parag.HandlerWebhook)
    http.HandleFunc("/paraglider/api/ticker/latest",  parag.HandlerLatestTicker)
    http.HandleFunc("/paraglider/api/ticker/",        parag.HandlerTicker)
    http.HandleFunc("/",                              handlerNotFound)

  log.Fatal(http.ListenAndServe(":"+port, nil))
  //http.ListenAndServe("127.0.0.1:8809", nil)
}
