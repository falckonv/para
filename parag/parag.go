// +heroku goVersion go1.11
// +heroku install ./...

package parag

import (
  "fmt"
  "time"
  "net/http"
  "regexp"
  "strconv"
  "strings"
  //"bytes"
  "io/ioutil"
  "encoding/json"
  "gopkg.in/mgo.v2"
  "gopkg.in/mgo.v2/bson"
  "github.com/marni/goigc"
)

const (
  HookURL    = "https://hooks.slack.com/services/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"
  // mongodb://<dbuser>:<dbpassword>@ds249503.mlab.com:49503/mongotrackdb
  // ds249503.mlab.com:49503

  DatabaseName           = "trackdb"
  RegTrackCollectionName = "tracks"
  DatabaseURL            = "mongodb://klorin:suppeergreit3@ds155663.mlab.com:55663/trackdb"
  HookCollectionName     = "hooks"
  //DatabaseURL            = "mongodb://klorin:suppeergreit3@ds249503.mlab.com:49503/mongotrackdb"
  /*
  DatabaseURL            = "127.0.0.1:27017/data/db/"
  DatabaseName           = "MongoTrackDB"
  RegTrackCollectionName = "tracks"
  */
  Version                = "v1"
  AboutTheApp            = "Service for reading .igc files online."
)


//  ----------  STRUCTS AND INTERFACES  -------------------------

/*
TrackStorage is an interface serving all access
to RegTrack-Track data
*/
type TrackStorage interface {
  Init()
  Add(reg RegTrack) error
  AddHook(hook Hook) error
  DeleteHook(string)
  Count() int
  //GetRegTrack(reg RegTrack) (RegTrack, bool)
  GetAll() []RegTrack
  GetHooks()[]Hook
  TransformIGC(u string) (RegTrack, bool)
  DeleteLastTracksInDB()
  DeleteAllTracksInDB()
  Get(RegTrack) (RegTrack, bool)
  //ApiFields(ap string) bool
  //NewID() (string)
}

/*
MongoTrackDB stores details about the mongoDB.
*/
type MongoTrackDB struct {
  DatabaseURL            string
  DatabaseName           string
  RegTrackCollectionName string
  HookCollectionName     string
}

/*
RegTrack object has all information about the track records.
RegTrack is the key in the database which keep Track objects.
*/
type RegTrack struct {
  Track
  TrURL    string     `json:"track_src_url" bson:"track_src_url"`
  TrID     int        `json:"id" bson:"id"` // Unique /igcinfo/api/<id>/
  Id    bson.ObjectId `bson:"_id,omitempty"`
  TrStamp  int64      `json:"timestamp" bson:"timestamp"`  // Nanoseconds
}

/*
Track object stores all flight track information.
Track is embedded in the RegTrack object
*/
type Track struct {
  HDate       string  `json:"H_date" bson:"H_date"`       // Date Header H-record
  Pilot       string  `json:"pilot" bson:"pilot"`         // Pilots name
  Glider      string  `json:"glider" bson:"glider"`       // Glider Type
  GliderID    string  `json:"glider_id" bson:"glider_id"` // Glider ID
  TrackLength float64 `json:"track_length" bson:"track_length"` // Calculated length (km)
}

type Hook struct {
  WebhookURL      map[string]string   `json:"webhookURL" bson:"webhookURL"`
  TriggerValue    map[string]string   `json:"minTriggerValue" bson:"minTriggerValue"`
  Text                string          `json:"text" bson:"text`
  Processing       time.Duration      `json:"processing_time" bson:"processing_time`
}

type HookGET struct {
  Tracknumber     string
  TrackArray			[]Track
}

type TimeST struct {
  Timestamp   bson.MongoTimestamp
  Timestamps  [] int64
}

type Ticker struct {
  TickerLast  time.Time
  TickerTimer time.Duration
}

// Points struct is not yet used.
type Points struct {
// TODO
}

/*
ServiceIGC is an object containing package information and
uptime for the web-service.
*/
type ServiceIGC struct {
  Uptime  string `json:"uptime"`
  Info    string `json:"info"`
  Version string `json:"version"`
}

// ----------- END OF STRUCTS AND INTERFACE  --------------------


//  ----------  GLOBAL VARIABLES  -------------------------------

/*
GlobalDB can be linked to any type of DB.
TrackStorage is an interface serving all access
to all RegTrack-Track data. Declaration in parag.go.
*/
var GlobalDB TrackStorage
/*
UpTimeStart is the global variable for storing the time of web launch
*/
var UpTimeStart time.Time
/*
LastID saves the last given id on flight tracks
*/
var LastID *int
/*
TRiD passes the id on flight tracks to web handler
NOT USED
*/
var TRID *string

//  ----------  END OF GLOBAL VARIABLES  ------------------------


//  ----------  START OF HELPER FUNCTIONS  ----------------------

/*
GetUpTime returns the duration since last launch of website as a string
*/
func GetUpTime() string {
  var start time.Duration
  step := time.Now()
  start = step.Sub(UpTimeStart)
  // Convert to smallest timevalue = xx seconds
  d := start.Truncate(time.Second)
  return d.String()
}

// APIInfo show the verion and uptime of the service.
func AppInfo()(ServiceIGC) {
  t      := GetUpTime()
  app    := ServiceIGC{
  Uptime :"\"uptime\":" + "\"" + t + "\"\n",
  Info   :"\"info\":\"" + AboutTheApp + "\"\n",
  Version:"\"version\":\"" + Version + "\"\n",
}
  return app
}

/*
APIFields verify correct url-api-extension on incoming
GET track information before sending request to DB
*/
func APIFields(ap string) bool {
  fields := map[string]int{"pilot": 1, "glider": 2, "glider_id": 3, "track_length": 4, "h_date": 5}
  _, ok := fields[ap]
  if !ok {
  return false
  }
  return true
}

/*
IDGenerator() give a RegTrack object an internal ID (API requests)
TODO: current version is not persistent
*/
func IDGenerator(reg RegTrack) RegTrack {
  a := *LastID + 1
  b := *LastID - 90000
    // Assign new ID and timestamp
    if reg.TrID == 0 {
    // Add new id
    reg.TrID = a
    // Add timestamp from current time as UnixNano
    t := time.Now()
    reg.TrStamp = t.UnixNano()
    *LastID = *LastID + 1
    return reg
    }
  // If something go wrong
  reg.TrID = b
  return  reg
}

//  ----------  END OF HELPER FUNCTIONS  ------------------------



//  ----------  START OF DATABASE FUNCTIONS  --------------------

//mongodb://<dbuser>:<dbpassword>@ds155663.mlab.com:55663/trackdb
/*
GetInfo() returns the database dialup info
*/
func GetInfo () (*mgo.DialInfo) {
  info := &mgo.DialInfo{
  Addrs: []string{"ds155663.mlab.com:55663"},
  Timeout: 60 * time.Second,
  Database: "trackdb",
  Username: "klorin",
  Password: "suppeergreit3",
  }
  return info
}



/*
Init initiates an collection of RegTrack-objects.
*/
func (db *MongoTrackDB) Init() {
/*
db.DatabaseURL  = "mongodb://klorin:suppeergreit3@ds155663.mlab.com:55663/trackdb"
db.DatabaseName = "trackdb"
db.RegTrackCollectionName =	"tracks"
*/

//session, err := mgo.Dial(db.DatabaseURL)
  session, err := mgo.DialWithInfo(GetInfo())
  if err != nil {
  fmt.Println(err.Error())
  panic(err)
  }
  defer session.Close()

  fmt.Println("Initialisation finished")

/*
    TESTING ONLY:
    s1 := RegTrack{TrID: 1, TrURL: "http://skypolaris.org/wp-content/uploads/IGS%20Files/Madrid%20to%20Jerez.igc", Track: Track{HDate: "2016-02-19 00:00:00 +0000 UTC", Pilot: "Miguel Angel Gordillo", Glider: "RV8", GliderID: "EC-XLL", TrackLength: 0}}
    s2 := RegTrack{TrID: 1, TrURL: "http://skypolaris.org/wp-content/uploads/IGS%20Files/Jarez%20to%20Senegal.igc", Track: Track{HDate: "2016-02-20 00:00:00 +0000 UTC", Pilot: "Miguel Angel Gordillo", Glider: "RV8", GliderID: "EC-XLL", TrackLength: 0}}
  GlobalDB.Add(s1)
    GlobalDB.Add(s2)
*/

}

// Add fills RegTrack objects to mongoDB.
func (db *MongoTrackDB) Add(reg RegTrack) error {
  session, err := mgo.DialWithInfo(GetInfo())
  //session, err := mgo.Dial(db.DatabaseURL)
  if err != nil {
  panic(err)
  }
  defer session.Close()
  //fmt.Println("Database name: " + db.DatabaseName)
  //fmt.Println("Collection name: " + db.RegTrackCollectionName)
  err = session.DB(db.DatabaseName).C(db.RegTrackCollectionName).Insert(reg)
  if err != nil {
  fmt.Printf("Error inserting track in Add(): %v", err.Error())
  return err
  }
  return nil
}

func (db *MongoTrackDB) AddHook(hook Hook) error {
  session, err := mgo.DialWithInfo(GetInfo())
  //session, err := mgo.Dial(db.DatabaseURL)
  if err != nil {
  panic(err)
  }
  defer session.Close()

  err = session.DB(db.DatabaseName).C(db.RegTrackCollectionName).Insert(hook)
  if err != nil {
  fmt.Printf("Error inserting webhook Add(): %v", err.Error())
  }
  return nil

}


func (db *MongoTrackDB) DeleteHook(string) {
  session, err := mgo.DialWithInfo(GetInfo())
  //session, err := mgo.Dial(db.DatabaseURL)
  if err != nil {
  panic(err)
  }
  defer session.Close()

  err = session.DB(db.DatabaseName).C(db.RegTrackCollectionName).Remove(bson.M{})
  if err != nil {
  fmt.Printf("Could not perform DeleteHook(): %v", err.Error())
  }
}


// Count the number of objects in the database
func (db *MongoTrackDB) Count() int {
  session, err := mgo.DialWithInfo(GetInfo())
  //session, err := mgo.Dial(db.DatabaseURL)
  if err != nil {
  panic(err)
  }
  defer session.Close()

  count, err := session.DB(db.DatabaseName).C(db.RegTrackCollectionName).Count()
  if err != nil {
  fmt.Printf("Error in Count(): %v", err.Error())
  return -1
  }
  return count
}

/*
GetRegTrack takes a RegTrack (key in map) if
match on either the url or the id of the object, a
complete RegTrack object with flight records is returned.
If RegTrack object is empty - false.
*/


/*
func (db *MongoTrackDB) GetRegTrack(reg RegTrack) (RegTrack, bool) {
  session, err := mgo.DialWithInfo(GetInfo())
  //session, err := mgo.Dial(db.DatabaseURL)
  if err != nil {
  panic(err)
  }
  defer session.Close()
  //reg        := RegTrack{}
  allWasGood := true
  //newRT := RegTrack{}
  var all []RegTrack
  // Check if incoming url via POST is already in DB
  if reg.TrURL != "" {
    // This search does not find the url even if it is registered...
  // Duplicates is stored
  // TODO: fix this
    err1 := session.DB(db.DatabaseName).C(db.RegTrackCollectionName).Find(bson.M{"track_src_url": reg.TrURL}).One(&all)
    if err1 != nil {
  //TODO error handling
    }
  // If url IS FOUND - do nothing (track already registered - return ID)
  //fmt.Printf("Setting of GetREGTRACK: Reg.url: %s.  Reg.TrID: %d, Pilot: %s \n\n", reg.TrURL, reg.TrID ,reg.Pilot)
  //fmt.Printf("Setting of GetREGTRACK: newRT.url: %s.  newRT.TrID: %d, Pilot: %s\n\n ", newRT.TrURL, newRT.TrID ,newRT.Pilot)
  }
  if reg.TrURL == "" {
  //fmt.Printf("TrURL2 (skal være tom): %s  TrID (skal ikke være tom)%d:\n\n ",reg.TrURL, reg.TrID)
    // Check if incoming API request via GET is in DB
    // Does not work if duplicate ids
    err2 := session.DB(db.DatabaseName).C(db.RegTrackCollectionName).Find(bson.M{"id": reg.TrID}).One(&all)
  // If id IS NOT FOUND - do nothing (return 404)
  if err2 != nil {
  allWasGood = false
  }
}

  // If either POST url is new or GET id is good: allWasGood --> true
  // reg-object
  //fmt.Printf("Reg.url: %s.  Reg.TrID: %d, Bool: %s ", reg.TrURL, reg.TrID ,allWasGood)
  return reg, allWasGood

}

*/

func (db *MongoTrackDB) DeleteLastTracksInDB () {
  session, err := mgo.DialWithInfo(GetInfo())
  //session, err := mgo.Dial(db.DatabaseURL)
  if err != nil {
  panic(err)
  }
  // Removes one single --> the last insert:
  err = session.DB(db.DatabaseName).C(db.RegTrackCollectionName).Remove(bson.M{})
  if err != nil {
  fmt.Printf("Could not perform DeleteLastTracksInDB(): %v", err.Error())
  }
  // Removes all inserts:
  //session.DB(db.DatabaseName).C(db.RegTrackCollectionName).RemoveAll(bson.M{})
}


func (db *MongoTrackDB) DeleteAllTracksInDB () {
  session, err := mgo.DialWithInfo(GetInfo())
  //session, err := mgo.Dial(db.DatabaseURL)
  if err != nil {
  panic(err)
  }
  err = session.DB(db.DatabaseName).DropDatabase()
  if err != nil {
  panic(err)
  }
}


func (db *MongoTrackDB)Get(reg RegTrack) (RegTrack, bool) {

  session, err := mgo.DialWithInfo(GetInfo())
  //session, err := mgo.Dial(db.DatabaseURL)
  if err != nil {
  panic(err)
  }
  defer session.Close()

  allIsGood := true
  var url string
  var id int
  url = reg.TrURL
  id  = reg.TrID

  switch {

  // Ask for latest timestamp by setting RegTrack.TrStamp = -1
  case reg.TrStamp == -1 :
  // Find the latest timestamp
  // TODO this is not working - wrong query for latest timestamp
  err1 := session.DB(db.DatabaseName).C(db.RegTrackCollectionName).Find(bson.M{"timestamp": -1}).One(&reg)
    // This should never happen unless DB is empty:
  if err1 != nil {
    allIsGood = false
    }

  case url != "" && id == 0 :
    //fmt.Printf("Get(): Track from POST (før) ID:%s, URL:%s, Pilot:%s, %s \n\n",reg.TrID, reg.TrURL, reg.Pilot, reg.HDate)
    err1 := session.DB(db.DatabaseName).C(db.RegTrackCollectionName).Find(bson.M{"track_src_url":url}).One(&reg)
    //fmt.Printf("Get(): Track from POST (etter) ID%s, URL%s, Pilot:%s, %s \n\n",reg.TrID, reg.TrURL, reg.Pilot, reg.HDate)
    // THE URL IS FOUND - return false (is alredy registered)
  if err1 == nil {
    allIsGood = false
    }
  case url == "" && id != 0 :
  //fmt.Printf("Get(): Track from GET (før) ID%s, URL%s, Pilot:%s, %s \n\n",reg.TrID, reg.TrURL, reg.Pilot, idString)
    err2 := session.DB(db.DatabaseName).C(db.RegTrackCollectionName).Find(bson.M{"id":id}).One(&reg)
  //fmt.Printf("Get(): Track from GET (etter) ID%s, URL%s, Pilot:%s, %s \n\n",reg.TrID, reg.TrURL, reg.Pilot, idString)
  if err2 != nil {
    //fmt.Printf("Get(): FALSE!!! ID%s, URL%s, Pilot:%s, %s \n\n",reg.TrID, reg.TrURL, reg.Pilot, reg.HDate)
    allIsGood = false
    }

  default: allIsGood = false
  }
  return reg, allIsGood
}

func (db *MongoTrackDB) GetHooks() []Hook {
  session, err := mgo.DialWithInfo(GetInfo())
  //session, err := mgo.Dial(db.DatabaseURL)
  if err != nil {
  panic(err)
  }
  defer session.Close()

  var hook []Hook
  err = session.DB(db.DatabaseName).C(db.HookCollectionName).Find(bson.M{}).All(&hook)
  if err != nil {
  return []Hook{}
  }
  return hook
}


/*
GetAll returns a slice of all RegTrack objects (the keys
in the database).
*/
func (db *MongoTrackDB) GetAll() []RegTrack {
  session, err := mgo.DialWithInfo(GetInfo())
  //session, err := mgo.Dial(db.DatabaseURL)
  if err != nil {
  panic(err)
  }
  defer session.Close()

  var all []RegTrack
  err = session.DB(db.DatabaseName).C(db.RegTrackCollectionName).Find(bson.M{}).All(&all)
  if err != nil {
  // If error return empty array
  return []RegTrack{}
  }
  return all
}

/*
TransformIGC takes an url from POST as a string and check if
it is a valid .igc flight record file. It it is, the content
is extracted. The track is stored as a RegTrack object
(url and trackID) with Tracks (metadata). Using Goigc/igc.
*/
func (db *MongoTrackDB) TransformIGC(u string) (RegTrack, bool) {
  session, err := mgo.DialWithInfo(GetInfo())
  //session, err := mgo.Dial(db.DatabaseURL)
  if err != nil {
  panic(err)
  }
  defer session.Close()
  allWasGood := true
  /*
     HARDCODED for testing:
     u = "http://skypolaris.org/wp-content/uploads/IGS%20Files/Jarez%20to%20Senegal.igc"
     u = "http://skypolaris.org/wp-content/uploads/IGS%20Files/Madrid%20to%20Jerez.igc"
  */
  // Check if url is alredy stored in DB (if record exists --> !ok)
  reg := RegTrack{}
  reg.TrURL = u
  // Get new regtrack-object with ID if not yet registered
  _, allWasGood = db.Get(reg)

  if !allWasGood {
  fmt.Println("The track is already in the database as track-00!" + strconv.Itoa(reg.TrID))
  return reg, allWasGood
  }

  if allWasGood {
    // IDGenerator --> Get new ID + timestamp as UnixNano
  reg = IDGenerator(reg)
  /*
  // If url is NOT ALREADY in the DB --> get ok
  _ , ok := db.GetRegTrack(reg)
  // Track is already in DB
  if !ok {
  allWasGood = false
*/

    //fmt.Printf("TransformIGC_2: Reg.url: %s.  Reg.TrID: %d, Bool: %s \n\n", reg.TrURL, reg.TrID ,allWasGood)
  // Break out - track IS ALREADY IN DB (Can extract ID from reg)

    // Goigc/igc package parsing the flight track data from .igc file
    track, err := igc.ParseLocation(u)
    if err != nil {
  /*
     TODO change this to an internal errorhandling.
     Current function only returns a RegTrack object - could be
     an empty one
     Treat the ERROR  - do not use err (no formatting directives).
     Should include an error as return value instead of bool!
  */
    //fmt.Errorf("Problem reading the received track file. Are you sure it is an .igc?", err)
    // Break out if parsing of igc-file fails
    return RegTrack{}, false
    }

    //fmt.Printf("TransformIGC: Track from parsing %s, %s, %s, %s, %s \n\n", track.Pilot, track.GliderType, track.GliderID, track.Date.String() )

  var dist float64
  // TODO implement function from received points.
  // Distance() is not working has to calculate from points
  dist = 0
  // Formatting the igc-data into RegTrack object according to JSON
  // Returning as a RegTrack-object reg
  //reg = RegTrack{TrID: 0, TrURL: u, Track: Track{HDate: track.Date.String(), Pilot: track.Pilot, Glider: track.GliderType, GliderID: track.GliderID, TrackLength: dist}}
  trc := Track{HDate: track.Date.String(), Pilot: track.Pilot, Glider: track.GliderType, GliderID: track.GliderID, TrackLength: dist}
  reg.Track = trc

  db.Add(reg)
   //fmt.Printf("TransformIGC_3: Reg.url: %s.  Reg.TrID: %d, Pilot: %s \n\n", reg.TrURL, reg.TrID ,reg.Pilot)
   //fmt.Printf("TransformIGC_4 Reg.url: %s.  Reg.TrID: %d, Bool: %s \n\n", reg.TrURL, reg.TrID ,allWasGood)
}
   //fmt.Printf("TransformIGC_5 Reg.url: %s.  Reg.TrID: %d, Bool: %s \n\n", reg.TrURL, reg.TrID ,allWasGood)
  return reg, allWasGood
}


//  ----------  END OF DATABASE FUNCTIONS  ----------------------



/*
 NB API for RegTrack - Tracks

 /* Initiate with dummies to test:
s1 := RegTrack{TrID: 1, TrURL: "http://skypolaris.org/wp-content/uploads/IGS%20Files/Madrid%20to%20Jerez.igc", Track: Track{HDate: "2016-02-19 00:00:00 +0000 UTC", Pilot: "Miguel Angel Gordillo", Glider: "RV8", GliderID: "EC-XLL", TrackLength: 0}}
s2 := RegTrack{TrID: 2, TrURL: "http://skypolaris.org/wp-content/uploads/IGS%20Files/Jarez%20to%20Senegal.igc", Track: Track{HDate: "2016-02-20 00:00:00 +0000 UTC", Pilot: "Miguel Angel Gordillo", Glider: "RV8", GliderID: "EC-XLL", TrackLength: 0}}

GlobalDB.Add(s1)
GlobalDB.Add(s2)

db.urlkeys[s1.TrURL] = 1
db.urlkeys[s2.TrURL] = 2
*/



//  ----------  START OF DISPLAYS  ------------------------------


// DISPLAY api/igc ---> json array of all Tracks of RegTracks (incl url, id)
func displayAllRegTracks(w http.ResponseWriter, db TrackStorage) {
  if db.Count() == 0 {
  json.NewEncoder(w).Encode([]RegTrack{})
  } else {
  add := make([]RegTrack, 0, db.Count())
  // Retreive all Track-objects by RegTrack key
  for _, trc := range db.GetAll() {
  add = append(add, trc)
  }
  json.NewEncoder(w).Encode(add)
  }
}

// DISPLAY api/igc ---> STRING array of all IDs
func displayAllRegTracksID(w http.ResponseWriter, db TrackStorage) {
  if db.Count() == 0 {
  json.NewEncoder(w).Encode([]string{})
  } else {
  add := make([]string, 0, db.Count())
  // Retreive all RegTracks (=key for Tracks)
  for _, reg := range db.GetAll() {
  add = append(add, strconv.Itoa(reg.TrID))
  }
  json.NewEncoder(w).Encode(add)
  }
}

func displayAllHooks(w http.ResponseWriter, db TrackStorage) {
  if db.Count() == 0 {
  json.NewEncoder(w).Encode([]Hook{})
  } else {
  add := make([]Hook, 0, db.Count())
  // Retreive all webhooks
  for _, reg := range db.GetHooks() {
  add = append(add, reg)
  }
  json.NewEncoder(w).Encode(add)
  }
}


// DISPLAY api/igc/<id> ---> one Track object (unnested) displayed
// The query for <id> identified by the split url response "u"
func displayOneRegTrack(w http.ResponseWriter, db TrackStorage, regInn RegTrack, field string) {
  //http.Header.Add(w.Header(), "date", time.RFC3339)
  // Find the reg in db with missing either RegTrack.Id or u (string)

  reg, ok := db.Get(regInn)

  //trc, ok := db.GetRegTrack(reg)
  //fmt.Printf("displayOneRegTrack_1: Reg.url: %s.  Reg.TrID: %d, Pilot: %s \n", reg.TrURL, reg.TrID ,reg.Pilot)
  //fmt.Printf("Setting of IGC TRAC: Reg.url: %s.  Reg.TrID: %d, Pilot: %s\n ", trc.TrURL, trc.TrID ,trc.Pilot)
  // If the reg cannot be confirmed as belonging to the database
  // it returns !ok (false)
  if !ok {
  http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)
  return
  }
  switch {
    case field == "":
    //http.Header.Add(w.Header(), "Content-Type", "application/json; charset=UTF-8")
    json.NewEncoder(w).Encode(reg)
    case field == "pilot":
    //http.Header.Add(w.Header(), "Content-Type", "text/plain")
    json.NewEncoder(w).Encode(reg.Pilot)
    case field == "glider":
    //	http.Header.Add(w.Header(), "Content-Type", "text/plain")
    json.NewEncoder(w).Encode(reg.Glider)
    case field == "glider_id":
    //http.Header.Add(w.Header(), "Content-Type", "text/plain")
    json.NewEncoder(w).Encode(reg.GliderID)
    case field == "h_date":
    //http.Header.Add(w.Header(), "content-type", "text/plain")
    json.NewEncoder(w).Encode(reg.HDate)
    case field == "track_length":
    //http.Header.Add(w.Header(), "Content-Type", "text/plain")
    json.NewEncoder(w).Encode(reg.TrackLength)
  default:
  //http.Header.Add(w.Header(), "Content-Type", "text/plain")
  http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
  return
  }
}


func displayLatestTimestamp(w http.ResponseWriter, db TrackStorage) {
  var regInn RegTrack
  regInn.TrStamp = -1
  var reg RegTrack
  reg, ok := db.Get(regInn)
  if !ok {
  // TODO better error handling - this should ONLY
  // happen if DB is empty
  http.Error(w, "", http.StatusNotFound)
  }
  // TODO: fix does not display last timestamp object
  json.NewEncoder(w).Encode(reg)
}

func displayLatestTicker(w http.ResponseWriter, db TrackStorage) {
  var ticker Ticker
  // TODO: make timer function to recieive last ticker
  json.NewEncoder(w).Encode(ticker.TickerLast)
}


// -----------  END OF DISPLAYS  --------------------------------



// -----------  START OF HANDLERS  ------------------------------


/*
HandlerRegTrack serves the /igcinfo/api/igc/ path
*/
func HandlerRegTrack(w http.ResponseWriter, r *http.Request) {
  http.Header.Add(w.Header(), "Date", time.RFC3339)
  var incomingURL string
  incomingURL = r.URL.Path
  //fmt.Println("Incoming URL in Handler Reg Track:  " + incomingURL)

  switch r.Method {
  // Receiving url as json {"url":"<url>"} in r.Body
    case "POST":
      http.Header.Add(w.Header(), "content-type", "application/json")
      // Placeholder for incomming url
      parts := strings.Split(r.URL.Path, "/")
      var le int
      le = len(parts)

      if le <= 3 || le >= 6 {
      http.Error(w, "", http.StatusNotFound)
      return
      }

      if incomingURL != "/paraglider/api/igc/"{
      http.Error(w, "POST can only be done at /paraglider/api/igc/", http.StatusBadRequest)
      }
      var urlIGC string
      // New RegTrack object to recieve the content of r.Body
      var reg RegTrack
      err := json.NewDecoder(r.Body).Decode(&reg)

      if err != nil {
      http.Error(w, err.Error(), http.StatusBadRequest)
      return
      }
      urlIGC = reg.TrURL

  /*
     REGEX checking of appropriate url-format:
     Tested regexp in http://regexr.com
     Possibly it has to be a http and not https?
     if both http/https: http?
     pattern := `^https?:\/\/[^\s$.?#].[^\s]*igc$`
     If http only: http:
  */
     pattern := `^http?:\/\/[^\s$.?#].[^\s]*igc$`
     rex := regexp.MustCompile(pattern)
     match := rex.MatchString(urlIGC)

     if !match {
     http.Error(w, "The url is not correctly formatted as .igc, please try again.", http.StatusBadRequest)
     return
     }

     // Chek if track-object already exists
     // Returns a filled RegTrack object and retreive the
     // track record linked to the sendt urlIGC
     regTrans, ok := GlobalDB.TransformIGC(urlIGC)
     // If track url is already in database.
     // THIS MESSAGE SHOULD NEVER APPEAR  it is stopped in Get()
     if !ok {
     http.Error(w, "This track id is already registered.", http.StatusBadRequest)
     return
     }
     //GlobalDB.Add(regTrans)
     // Formatted according to specifications {"id":"<id>"}
     fmt.Fprintf(w, "{\"id\":\"%d\"}", regTrans.TrID)
     return

    // GET for api/igc
    // Returns/display an array of all tracks ids as JSON:
    case "GET":
      // Getting the right path and checing for correct format/names
      // Making a RegTrack object to receive track id (TrID) from URL
      // part api/igc/<track-xxxx>
      var reg RegTrack
      // Make a slice of url path components
      // root empty/empty <api>/empty<igc>/empty<track-(id)>/empty/<field>
      // parts[2] = api, parts[3] = igc, parts[4] = track, part[5] = field
      parts := strings.Split(r.URL.Path, "/")
      // Pattern of <track-xxxx> with exactly 4 digits 0-9 (starting at 0001)
      // Changing from {3} to {2} decrease digits to 3
      var le int
      le = len(parts)
      //fmt.Fprintf(w, "Parts[0]%s, [1]%s,\n [2]%s", parts[0],parts[1], parts[2])

      if  incomingURL == "/paraglider/api/igc" || incomingURL == "/paraglider/api/igc/" {
      http.Header.Add(w.Header(), "content-type", "application/json")
      // Can also use:
      // w.Header().Add("content-type", "application/json")

      // ID as string array [] = empty
      displayAllRegTracks(w, GlobalDB)
      // Track-objects as an array {} when empty
      //displayAllRegTracks(w, GlobalDB)
      }

      if le <= 3 || le >= 8 {
      http.Error(w, "", http.StatusNotFound)
      return
      }

      if incomingURL == "/paraglider/api/" {
      HandlerAPIInfo(w,r)
      return
      }

      //patternTrack := `^track-[0-9]{3}[0-9]$`
      //rex   := regexp.MustCompile(patternTrack)
      rex := regexp.MustCompile("^track-[0-9]{3}[0-9]$")
      match := rex.MatchString(parts[4])
      if !match {
      // CURRENTLY FOR TESTING ONLY:
      //fmt.Fprintf(w, "Parts: 1%s, 2%s,\n 3%s, 4%s,\n 5%s, 6%s, 7%s", parts[0],parts[1], parts[2], parts[3], parts[4], parts[5], parts[6])
      http.Error(w, "The url you ask for is not correct in this api.\n Correct format: api/igc/<track-xxx>/<field>.", http.StatusBadRequest)
      return
      }
      // Extracting TrID as an integer from the API url part <track-xxxx>
      numString := strings.Split(parts[4], "-")
      numInt, err := strconv.Atoi(numString[1])
        if err != nil {
        http.Error(w, "The tracknumber is not formatted correctly: api/igc/<track-00xx> with 0´s in front of number.", http.StatusBadRequest)
        return
        }
      // Pass the trackid part of the url to a RegTrack object
      //reg := RegTrack{}
      reg.TrID = numInt

      // ALL GET REQUESTS for api/igc/<track-xxxx>
      // Returns/display an array a single track by id as JSON
      if le == 5 || le == 6 {
      http.Header.Add(w.Header(), "content-type", "application/json")
      //w.Header().Add("content-type", "application/json")
      // Return and display the trackrecord data as JSON in body response
      displayOneRegTrack(w, GlobalDB, reg, "")
      }
      // Printing confirm that the number conversion is ok and that parts 4 = track-00xx
      //fmt.Fprintf(w, "Did the string number convert? String:%s, Int:%d\n Parts[4]: %s.", numString[1], numInt, parts[4])

      // Get the /api/igc/track-xxxx/<field>
      if le == 7 {
      http.Header.Add(w.Header(), "Content-Type", "text/plain; charset=UTF-8")
      //http.Header.Add(w.Header(), "Uptime", GetUpTime())
      //var field string
      field := strings.ToLower(parts[5])

        // Checing if field name is valid
        if !APIFields(field) {
        http.Error(w, "The field you asked for is not in this API.", http.StatusBadRequest)
        return
        }
      // ONLY FOR TEST:
      //fmt.Fprintf(w,"HandlerRegTrack_1:Field: Tekst:%s, Type:%t,\n Variable:%v, Parts[5]:%s, Parts[4]:%s\n\n", field, field, field, parts[5], parts[4])
      //fmt.Fprintf(w, "HandlerRegTrack_2:Parts: 0%s, 1%s,\n 2%s, 3%s,\n 4%s, 5%s\n\n", parts[0],parts[1], parts[2], parts[3], parts[4], parts[5])
      // Display <track-xxxx/Pilot/
      fmt.Fprintf(w, "track-000%s \n%s: ", strconv.Itoa(reg.TrID), field)

      displayOneRegTrack(w, GlobalDB, reg, field)
      }

    case "DELETE":
      // Removes last inserted item
      GlobalDB.DeleteLastTracksInDB()

  default:
    http.Error(w, "This is not an option in this API.", http.StatusBadRequest)
    return
  }
}

/*
TODO INCLUDE to safeguard concurrency for DB input
SafeTrack sync.Mutex 		// Protects session
Maxlifetime int64				// Session duration
*/

// HandlerAPIInfo show the verion and uptime of the service.
func HandlerAPIInfo(w http.ResponseWriter, r *http.Request) {
  w.Header().Add("content-type", "application/json")
  http.Header.Add(w.Header(), "Date", time.RFC3339)
  http.Header.Add(w.Header(), "Uptime", GetUpTime())
  fmt.Fprint(w, AppInfo())
}


/*
HandlerAdmin serves the /admin/api/tracks/ and by r.Method DELETE
the database is deleted with a response of how many tracks which
were deleted.
*/
func HandlerAdmin(w http.ResponseWriter, r *http.Request) {
  w.Header().Add("content-type", "text/plain; charset=UTF-8")
  http.Header.Add(w.Header(), "Date", time.RFC3339)
  http.Header.Add(w.Header(), "Uptime", GetUpTime())
  switch r.Method {
    case "DELETE": count := GlobalDB.Count()
      GlobalDB.DeleteAllTracksInDB()
        if GlobalDB.Count() > 0 {
        http.Error(w, "All tracks could not be deleted.", http.StatusBadRequest)
        return
        }
      fmt.Fprint(w, "All tracks in DB is deleted. Number of tracks: ", count)
  default:
    http.Error(w, "", http.StatusNotFound)
    return
  }
}


/*
HandlerAdminTrackC serves the /admin/api/tracks_count. By the r.Method GET
it returns the number of flight track records in the database.
*/
func HandlerAdminTrackC(w http.ResponseWriter, r *http.Request) {
  w.Header().Add("content-type", "text/plain; charset=UTF-8")
  http.Header.Add(w.Header(), "Date", time.RFC3339)
  http.Header.Add(w.Header(), "Uptime", GetUpTime())
  switch r.Method {
    case "GET": count := GlobalDB.Count()
    fmt.Fprint(w, "Number of tracks in DB: ", count)
  default:
    http.Error(w, "", http.StatusNotFound)
    return
  }

}

/*
HandlerWebhook serves the /paraglider/api/igc/webhook/new_track
*/
func HandlerWebhook(w http.ResponseWriter, r *http.Request){
  w.Header().Add("content-type", "application/json; charset=UTF-8")
  http.Header.Add(w.Header(), "Date", time.RFC3339)
  http.Header.Add(w.Header(), "Uptime", GetUpTime())

/*
POST /api/webhook/new_track/
What: Registration of new webhook for notifications about tracks being added
to the system. Returns the details about the registration. The webhookURL
is required parameter of the request. The minTriggerValue is optional integer,
that defaults to 1 if ommited. It indicated the frequency of updates - after
how many new tracks the webhook should be called.
Response type: application/json
Response code: 200 or 201 if everything is OK, appropriate error code otherwise.
Request
{
    "webhookURL": {
      "type": "https://hooks.slack.com/services/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"
    },
    "minTriggerValue": {
      "type": 3
    }
}

Example, that registers a webhook that should be trigger for every two new
tracks added to the system.
{
    "webhookURL": https://hooks.slack.com/services/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,
    "minTriggerValue": 2
}

Response

The response body should contain the id of the created resource (aka webhook
registration), as string. Note, the response body will contain only the created
id, as string, not the entire path; no json encoding. Response code upon
success should be 200 or 201.

nvoking a registered webhook
When invoking a registered webhook, use POST with the webhookURL and the
following payload specification, in human readable format:

# example for Slack
{
   "text": <the body as string>
}

the body as string should contain 3 pieces of data: the timpestamp of the
track added the latest, the new tracks ids (the ones added since the webhook
was triggered last time), and the processing time it took your server to
actually prepare and run the trigger.
Notes:

the body should include only the NEW tracks ids. Not the entire collection!
the exact return format will depend on the webhook system that you use. It
differs between Discord, Slack or other system that you want to us. Using
Discord or Slack is encouraged. You can use Slack format with Discord if you
append "/slack" at the end of the webhook url (thanks Adrian L. Lange for the
heads up!)
example body: "Latest timestamp: 6742924356, 2 new tracks are: id45, id46.
(processing: 2s 548ms)"

In case you want to implement your own trigger handler, you could use this
body instead of "human readable" output with a single text field, like that:

{
   "t_latest": <latest added timestamp of the entire collection>,
   "tracks": [<id1>, <id2>, ...],
   "processing": <time in ms of how long it took to process the request>
}
Note, this is not required, and the use of Discord or Slack for human-debugging is encouraged.


GET /api/webhook/new_track/<webhook_id>

What: Accessing registered webhooks. Registered webhooks should be accessible using the GET method and the webhook id generated during registration.
Response type: application/json
Response code: 200 or 201 if everything is OK, appropriate error code otherwise.
Response body

{
    "webhookURL": {
      "type": "string"
    },
    "minTriggerValue": {
      "type": "number"
    }
}

DELETE /api/webhook/new_track/<webhook_id>
What: Deleting registered webhooks. Registered webhooks can further be
deleted using the DELETE method and the webhook id.
Response type: application/json
Response code: 200 or 201 if everything is OK, appropriate error code otherwise.
Response body:

{
    "webhookURL": {
      "type": "string"
    },
    "minTriggerValue": {
      "type": "number"
    }
}

*/

  switch r.Method {
    case "POST":
      var hook Hook
      err := json.NewDecoder(r.Body).Decode(&hook)
        if err != nil {
        http.Error(w, err.Error(), http.StatusBadRequest)
        return
        }
      //fmt.Println("response Status:", r.Status)
      fmt.Println("response Headers:", r.Header)
      body, _ := ioutil.ReadAll(r.Body)
      fmt.Fprint(w, string(body))

    case "GET":
      displayAllHooks(w, GlobalDB)

    case "DELETE":

    default:
      http.Error(w, "This is not an option in this API.", http.StatusBadRequest)
      return
    }
/*
Response = incoming webhooks (json response)
{
    "ok": true,
    "access_token": "xoxp-XXXXXXXX-XXXXXXXX-XXXXX",
    "scope": "identify,bot,commands,incoming-webhook,chat:write:bot",
    "user_id": "XXXXXXXX",
    "team_name": "Your Team Name",
    "team_id": "XXXXXXXX",
    "incoming_webhook": {
        "channel": "#channel-it-will-post-to",
        "channel_id": "C05002EAE",
        "configuration_url": "https://teamname.slack.com/services/BXXXXX",
        "url": "https://hooks.slack.com/TXXXXX/BXXXXX/XXXXXXXXXX"
    }
}
*/

}

/*
HandlerLatestTicker serves the /paraglider/api/igc/ticker/latest/
It returns the last time as timestamp of the latest added flight tracks.
*/
func HandlerLatestTicker (w http.ResponseWriter, r *http.Request) {
  w.Header().Add("content-type", "application/json; charset=UTF-8")
  http.Header.Add(w.Header(), "Date", time.RFC3339)
  http.Header.Add(w.Header(), "Uptime", GetUpTime())

  switch r.Method {
    case "GET":
      displayLatestTicker(w, GlobalDB)
  default:
    http.Error(w, "", http.StatusNotFound)
    return
  }
/*
What: returns the timestamp of the latest added track
Response type: text/plain
Response code: 200 if everything is OK, appropriate error code otherwise.
Response: <timestamp> for the latest added track
*/

}

/*
HandlerTicker serves the /paraglider/api/ticker/
It returns the last time as timestamp of the latest added flight tracks.
*/
func HandlerTicker (w http.ResponseWriter, r *http.Request) {
  w.Header().Add("content-type", "application/json; charset=UTF-8")
  http.Header.Add(w.Header(), "Date", time.RFC3339)
  http.Header.Add(w.Header(), "Uptime", GetUpTime())
  switch r.Method {
    case "GET":
    displayLatestTimestamp(w, GlobalDB)
    default:
    http.Error(w, "", http.StatusNotFound)
      return
 }
}
 //  -------------- END OF HANDLERS -----------------------------

  /*
GET /api/ticker/

What: returns the JSON struct representing the ticker for the IGC tracks. The first track returned should be the oldest. The array of track ids returned should be capped at 5, to emulate "paging" of the responses. The cap (5) should be a configuration parameter of the application (ie. easy to change by the administrator).
Response type: application/json
Response code: 200 if everything is OK, appropriate error code otherwise.
Response
  {
  "t_latest": <latest added timestamp>,
  "t_start": <the first timestamp of the added track>, this will be the oldest track recorded
  "t_stop": <the last timestamp of the added track>, this might equal to t_latest if there are no more tracks left
  "tracks": [<id1>, <id2>, ...],
  "processing": <time in ms of how long it took to process the request>
  }


  GET /api/ticker/<timestamp>
What: returns the JSON struct representing the ticker for the IGC tracks. The first returned track should have the timestamp HIGHER than the one provided in the query. The array of track IDs returned should be capped at 5, to emulate "paging" of the responses. The cap (5) should be a configuration parameter of the application (ie. easy to change by the administrator).
Response type: application/json
Response code: 200 if everything is OK, appropriate error code otherwise.
Response:
  {
  "t_latest": <latest added timestamp>,
  "t_start": <the first timestamp of the added track>, this will be the oldest track recorded
  "t_stop": <the last timestamp of the added track>, this might equal to t_latest if there are no more tracks left
  "tracks": [<id1>, <id2>, ...],
  "processing": <time in ms of how long it took to process the request>
  }
  */



// -------- END OF API ------------------------------------------
