// +heroku goVersion go1.11
// +heroku install ./...

package parag

import (
	"fmt"
	"encoding/json"
	"gopkg.in/mgo.v2"
	//"gopkg.in/mgo.v2/bson"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"time"
	//"go/build"
)

/*
Senegal:
"http://skypolaris.org/wp-content/uploads/IGS%20Files/Jarez%20to%20Senegal.igc"
Madrid:
"http://skypolaris.org/wp-content/uploads/IGS%20Files/Madrid%20to%20Jerez.igc"
*/

// TODO  FIX:
// TEST IS FAILING PARTLY BECAUSE THE COUNTER GIVES ADDING TO DB FAILURES


// Testdatabase
const (
//DatabaseURL = "mongodb://klorin:suppeergreit3@ds249503.mlab.com:49503/mongotrackdb"

)


func setupDB(t *testing.T) *MongoTrackDB {
	db := MongoTrackDB {
		"mongodb://klorin:suppeergreit3@ds155663.mlab.com:55663/trackdb",
		"trackdb",
		"tracks",
	}
	session, err := mgo.DialWithInfo(GetInfo())
	if err != nil {
		fmt.Println(err.Error())
		panic(err)
	}
	defer session.Close()
	return &db
}

func tearDownDB(t *testing.T, db *MongoTrackDB) {
	session, err := mgo.Dial(db.DatabaseURL)
	defer session.Close()
	if err != nil {
		t.Error(err)
	}

	err = session.DB(db.DatabaseName).DropDatabase()
	if err != nil {
		t.Error(err)
	}
}

// Testing the Upsert statement
func TestMongo_Upsert(t *testing.T) {
	db := setupDB(t)
	defer tearDownDB(t, db)

	db.Init()
	s1 := RegTrack{TrID: 1, TrURL: "http://skypolaris.org/wp-content/uploads/IGS%20Files/Madrid%20to%20Jerez.igc", Track: Track{HDate: "2016-02-19 00:00:00 +0000 UTC", Pilot: "Miguel Angel Gordillo", Glider: "RV8", GliderID: "EC-XLL", TrackLength: 0}}
	//student := Student{Name: "Tom", Age: 21, StudentID: "id1"}
  db.Add(s1)

	session, err := mgo.Dial(db.DatabaseURL)
	if err != nil {
		panic(err)
	}
	defer session.Close()

	var resultTrack RegTrack
	upsertInfo, err := session.DB(db.DatabaseName).C(db.RegTrackCollectionName).Upsert(resultTrack, resultTrack)

	if err != nil {
		fmt.Printf("error in FindId(): %v", err.Error())
		return
	}

	id := upsertInfo.UpsertedId
	err = session.DB(db.DatabaseName).C(db.RegTrackCollectionName).FindId(id).One(&resultTrack)

	if err != nil {
		fmt.Printf("error in FindId(): %v", err.Error())
		return
	}

	if db.Count() != 1 {
		t.Error("Adding new track failed.")
	}
}

// Testing the Insert and FindId statements
func TestMongoDB_Insert(t *testing.T) {
	db := setupDB(t)
	defer tearDownDB(t, db)

	db.Init()

  if db.Count() != 0 {
		t.Error("Database not properly initialized. Track count() should be 0.")
	}

	track := RegTrack{TrID: 1, TrURL: "http://skypolaris.org/wp-content/uploads/IGS%20Files/Madrid%20to%20Jerez.igc", Track: Track{HDate: "2016-02-19 00:00:00 +0000 UTC", Pilot: "Miguel Angel Gordillo", Glider: "RV8", GliderID: "EC-XLL", TrackLength: 0}}
	db.Add(track)

	session, err := mgo.Dial(db.DatabaseURL)
	if err != nil {
		panic(err)
	}
	defer session.Close()

	var resultTrack RegTrack
	err = session.DB(db.DatabaseName).C(db.RegTrackCollectionName).FindId(track.TrID).One(&resultTrack)

	if err != nil {
		fmt.Printf("Error in FindId(): %v", err.Error())
		return
	}

	if db.Count() != 1 {
		t.Error("Adding new track failed.")
	}
}

func TestTrackMongoTrackDB_Get(t *testing.T) {
	db := setupDB(t)
	defer tearDownDB(t, db)

	db.Init()
	if db.Count() != 0 {
		t.Error("database not properly initialized. student count() should be 0.")
		return
	}
  track := RegTrack{TrID: 1, TrURL: "http://skypolaris.org/wp-content/uploads/IGS%20Files/Madrid%20to%20Jerez.igc", Track: Track{HDate: "2016-02-19 00:00:00 +0000 UTC", Pilot: "Miguel Angel Gordillo", Glider: "RV8", GliderID: "EC-XLL", TrackLength: 0}}
	db.Add(track)

	if db.Count() != 1 {
		t.Error("Adding new track failed.")
	}

	newTrack, ok := db.Get(track)
	if !ok {
		t.Error("Couldn't find track-0001")
	}

	if newTrack.TrURL != track.TrURL ||
		newTrack.Pilot != track.Pilot ||
		newTrack.GliderID != track.GliderID {
		t.Error("Track do not match")
	}

	all := db.GetAll()
	if len(all) != 1 || all[0].TrID != track.TrID {
		t.Error("GetAll() doesn't return proper slice of all the items")
	}
}



// ----- END OF MONGODB TEST



func Test_multipleRegTracks(t *testing.T) {
	db := setupDB(t)
	defer tearDownDB(t, db)

	db.Init()
	// Two first are real online igc-tracks
	s1 := RegTrack{TrID: 1, TrURL: "http://skypolaris.org/wp-content/uploads/IGS%20Files/Madrid%20to%20Jerez.igc", Track: Track{HDate: "2016-02-19 00:00:00 +0000 UTC", Pilot: "Miguel Angel Gordillo", Glider: "RV8", GliderID: "EC-XLL", TrackLength: 0}}
	s2 := RegTrack{TrID: 2, TrURL: "http://skypolaris.org/wp-content/uploads/IGS%20Files/Jarez%20to%20Senegal.igc", Track: Track{HDate: "2016-02-20 00:00:00 +0000 UTC", Pilot: "Miguel Angel Gordillo", Glider: "RV8", GliderID: "EC-XLL", TrackLength: 0}}
	//s3 := RegTrack{TrID: 3, TrURL: "http://example.com/igc/track3.igc", Track: Track{HDate: "2017-04-09", Pilot: "Marius Muller", Glider: "Theodor", GliderID: "AIKK-12", TrackLength: 34.6}}

		db.Add(s1)
		db.Add(s2)

	// Checking if the DB has the same number of items as testData
	if db.Count() != 2 {
		t.Errorf("Wrong number of tracks %d, DB count is %d", 2, db.Count())
	}
}

/* TODO
func Test_handlerRegTrackDeleted(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(HandlerRegTrack))
	defer ts.Close()

	// create a request to our mock HTTP server
	//    in our case it means to create DELETE request
	client := &http.Client{}
	reg, err := http.NewRequest(http.MethodDelete, ts.URL, nil)
	if err != nil {
		t.Errorf("Error constructing the DELETE request, %s", err)
	}

	resp, err := client.Do(reg)
	if err != nil {
		t.Errorf("Error executing the DELETE request, %s", err)
	}

	// check if the response from the handler is what we expect
	if resp.StatusCode != http.StatusNotImplemented {
		t.Errorf("Expected StatusCode %d, received %d", http.StatusNotImplemented, resp.StatusCode)
	}
}

*/






func Test_handlerRegTrack_malformedURL(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(HandlerRegTrack))
	defer ts.Close()

	testCases := []string{
		ts.URL,
		ts.URL + "/paraglider/api/ig",
		ts.URL + "/paraglider/igc/",
		ts.URL + "/paraglider/api/igc/rubi",
		ts.URL + "/paraglider",
		ts.URL + "/paraglider/api/igc/track-",
	}
	for _, tstring := range testCases {
		resp, err := http.Get(tstring)
		if err != nil {
			t.Errorf("Error making the GET request, %s", err)
		}

		if resp.StatusCode != http.StatusBadRequest {
			t.Errorf("For route: %s, expected StatusCode %d, received %d", tstring, http.StatusBadRequest, resp.StatusCode)
			return
		}
	}
}


// GET /track/ empty array back
func Test_handlerRegTrack_getAllTracks_empty(t *testing.T) {

	db := setupDB(t)
	defer tearDownDB(t, db)

	db.Init()

	ts := httptest.NewServer(http.HandlerFunc(HandlerRegTrack))
	defer ts.Close()

	resp, err := http.Get(ts.URL + "/paraglider/api/igc")
	if err != nil {
		t.Errorf("Error making the GET request, %s", err)
	}

	if resp.StatusCode != http.StatusOK {
		t.Errorf("Expected StatusCode %d, received %d", http.StatusOK, resp.StatusCode)
		return
	}

	var a []interface{}
	err = json.NewDecoder(resp.Body).Decode(&a)
	if err != nil {
		t.Errorf("Error parsing the expected JSON body. Got error: %s", err)
	}

	if len(a) != 0 {
		t.Errorf("Excpected empty array, got %s", a)
	}
}





func Test_handlerRegTrack_displayAllRegTracks(t *testing.T) {
	db := setupDB(t)
	defer tearDownDB(t, db)

	db.Init()

	ts := httptest.NewServer(http.HandlerFunc(HandlerRegTrack))
	defer ts.Close()

	// TODO Check this - does not seem to get the url-body like a Track object
	// as supposed - to substitute: make a hardcoded body to test. TrcArr seems
	// to be empty(?)
	// Getting the test url into the database via web:
	resp, err := http.Post(ts.URL+"/paraglider/api/igc", "application/json", strings.NewReader("{'url':'http://skypolaris.org/wp-content/uploads/IGS%20Files/Madrid%20to%20Jerez.igc'} "))
	if err != nil {
		t.Errorf("Error creating the POST request, %s", err)
	}
	// Check if it is registered - if so this will be a duplicate:
	testURL := "http://skypolaris.org/wp-content/uploads/IGS%20Files/Madrid%20to%20Jerez.igc"
	testRegTrack, _ := GlobalDB.TransformIGC(testURL)
	err = GlobalDB.Add(testRegTrack)

	if err != nil {
		t.Errorf("The transforming from igc url to json regtrack-object failed.%s", err)
	}
	resp, err = http.Get(ts.URL + "/paraglider/api/igc/track-0001")
	if err != nil {
		t.Errorf("Error making the GET request, %s", err)
	}

	if resp.StatusCode != http.StatusOK {
		t.Errorf("Expected StatusCode %d, received %d", http.StatusOK, resp.StatusCode)
		return
	}

	//var trcArr []Track

	trc := Track{HDate: "2016-02-19 00:00:00 +0000 UTC", Pilot: "Miguel Angel Gordillo", Glider: "RV8", GliderID: "EC-XLL", TrackLength: 0}



	if trc.Pilot != testRegTrack.Track.Pilot || trc.Glider != testRegTrack.Track.Glider || trc.GliderID != testRegTrack.Track.GliderID {
		t.Errorf("Tracks and info do not match! Got: %v, Expected: %v\n", trc, testRegTrack.Track)
	}

}




// GET /igcinfo/api/igc/track-0004
// Single new added track (initiaties 3 + add Madrid)
func Test_HandlerRegSingleTrackGetSingleTrackMadrid(t *testing.T) {
	db := setupDB(t)
	defer tearDownDB(t, db)

	db.Init()

	testURL := "http://skypolaris.org/wp-content/uploads/IGS%20Files/Madrid%20to%20Jerez.igc"
	testRegTrack, _ := GlobalDB.TransformIGC(testURL)
	GlobalDB.Add(testRegTrack)
	ts := httptest.NewServer(http.HandlerFunc(HandlerRegTrack))
	defer ts.Close()

	// --------------
	resp, err := http.Get(ts.URL + "/paraglider/api/igc/track-0005")
	if err != nil {
		t.Errorf("Error making the GET request, %s", err)
	}

	// --------------
	resp, err = http.Get(ts.URL + "/paraglider/api/igc/track-0001")
	if err != nil {
		t.Errorf("Error making the GET request to track Madrid, %s", err)
	}

	if resp.StatusCode != http.StatusOK {
		t.Errorf("Expected StatusCode %d, received %d", http.StatusOK, resp.StatusCode)
		return
	}

	var a Track
	err = json.NewDecoder(resp.Body).Decode(&a)
	if err != nil {
		t.Errorf("Error parsing the expected JSON body. Got error: %s", err)
	}

	if a.Pilot != testRegTrack.Pilot || a.Glider != testRegTrack.Glider || a.GliderID != testRegTrack.GliderID {
		t.Errorf("Track-0001 do not match! Got: %v, Expected: %v\n", a, testRegTrack)
	}
}




func Test_handlerRegTrack_POST(t *testing.T) {
	db := setupDB(t)
	defer tearDownDB(t, db)

	db.Init()

	ts := httptest.NewServer(http.HandlerFunc(HandlerRegTrack))
	defer ts.Close()


	// Testing sending correctly formatted url as json
	resp, err := http.Post(ts.URL+"/paraglider/api/igc", "application/json", strings.NewReader("{'url':'http://skypolaris.org/wp-content/uploads/IGS%20Files/Madrid%20to%20Jerez.igc'} "))
	if err != nil {
		t.Errorf("Error creating the POST request, %s", err)
	}

	if resp.StatusCode != http.StatusBadRequest {
		t.Errorf("Expected StatusCode %d, received %d", http.StatusBadRequest, resp.StatusCode)
	}

	// Testing proper JSON body
	turl := `{"url":"http://skypolaris.org/wp-content/uploads/IGS%20Files/Madrid%20to%20Jerez.igc"}`

	resp, err = http.Post(ts.URL+"/paraglider/api/igc/", "application/json", strings.NewReader(turl))
	if err != nil {
		t.Errorf("Error creating the POST request, %s", err)
	}

	if resp.StatusCode != http.StatusOK {
		all, _ := ioutil.ReadAll(resp.Body)
		t.Errorf("Expected StatusCode %d, received %d, Body: %s",
			http.StatusOK, resp.StatusCode, all)
	}
	// Add the new url (Madrid) to the DB (Not checking the add function here)
	testRegTrack, _ := GlobalDB.TransformIGC(turl)
	GlobalDB.Add(testRegTrack)

	// Trying to add same url-track a second time
	resp, err = http.Post(ts.URL+"/paraglider/api/igc", "application/json", strings.NewReader(turl))
	if err != nil {
		t.Errorf("Error creating the POST request, %s", err)
	}

	if resp.StatusCode != http.StatusBadRequest {
		all, _ := ioutil.ReadAll(resp.Body)
		t.Errorf("Expected StatusCode %d, received %d, Body: %s",
			http.StatusBadRequest, resp.StatusCode, all)
	}

	// Testing malformed JSON body
	wrongTrack := "{'urls':'http://skypolaris.org/wp-content/uploads/IGS%20Files/Madrid%20to%20Jerez.igc'}"

	resp, err = http.Post(ts.URL+"/paraglider/api/igc", "application/json", strings.NewReader(wrongTrack))
	if err != nil {
		t.Errorf("Error creating the POST request, %s", err)
	}

	if resp.StatusCode != http.StatusBadRequest {
		all, _ := ioutil.ReadAll(resp.Body)
		t.Errorf("Expected StatusCode %d, received %d, Body: %s",
			http.StatusBadRequest, resp.StatusCode, all)
	}
}
